#Práctico número 2 de visión por computadora"

import cv2

umbral = 170                                             #Valor de umbral el cuál a partir de él pone el pixel en negro 
max_value = 255                                          # 255 = píxel negro
min_value = 0                                            # 0 = píxel blanco

img = cv2.imread('LesPaul.png', 0)                          #Lee la imagen y la guarda en escala de grises

for i, row in enumerate(img):
    for j, col in enumerate(row):                        #Estos dos for recorren todos los píxeles de la imagen y verifica si el valor es mayor o menos a un umbral
        if img[i][j] >= umbral:
            img[i][j] = max_value
        else:
            img[i][j] = min_value
        
cv2.imwrite('imagenResultante1.png', img)
cv2.imshow('imagenResultante1', img)                     #Muestra la imagen en una ventana nueva
cv2.waitKey(0)

