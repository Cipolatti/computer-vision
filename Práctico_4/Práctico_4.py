#Práctico número 4 de visión por computadora - 
#Obtener el ancho y alto de las imágenes capturadas usando los macros de OpenCV

import cv2

fps = 30

cap = cv2.VideoCapture(0)
fourcc = cv2.VideoWriter_fourcc('X', 'V', 'I', 'D')
width = int(cap.get(3))                                                     #obtiene el CAP_PROP_FRAME_WIDTH
heigth = int(cap.get(4))                                                    #Obtiene el CAP_PROP_FRAME_HEIGTH
print('My webcam width is: ', width, 'and heigth is: ', heigth)
out = cv2.VideoWriter('output.mp4', fourcc, fps,(width, heigth))

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret is True:
        out.write(frame)
        cv2.imshow('frame', frame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    else:
        break

cap.release()
out.release()
cv2.destroyAllWindows()
