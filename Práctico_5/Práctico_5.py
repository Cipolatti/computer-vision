# Práctico 5 de visión por computadora
# Recorta un rectángulo de una imagen y lo guarda en otra imagen

import cv2
import numpy as np

drawing = False
ix, iy = -1, -1


def draw(
    event, x, y, flags, param
):    # Función que se llama cuándo ocurre algún evento proveniente del mouse
    global ix, iy, drawing, mode, crop_img
    if event == cv2.EVENT_LBUTTONDOWN:    # Cuándo se aprieta el botón
        drawing = True
        ix, iy = x, y

    elif event == cv2.EVENT_LBUTTONUP:    # Cuándo suelta el botón
        drawing = False
        cv2.rectangle(imagen_entrante, (ix, iy), (x, y), (255, 0, 0), 3)
        crop_img = imagen_entrante[iy:y, ix:x]


imagen_entrante = cv2.imread("LesPaul.png", 1)    # Lee la imagen

cv2.namedWindow("image")
cv2.setMouseCallback("image", draw)

while 1:

    cv2.imshow("image", imagen_entrante)
    k = cv2.waitKey(1) & 0xFF

    if k == ord("q"):    # Si se presiona 'q' cierra el programa
        break

    elif k == ord(
            "g"
    ):    # Si se presiona 'g', guarda la imagen recortada y cierra el programa
        if not cv2.imwrite("Imagen_cortada.png", crop_img):
            print("No se pudo escribir la imagen")
        else:
            print("Se ha guardado la sección de la imagen marcada")
            break

    elif k == ord("r"):    # Si se presiona 'r', restaura la imagen
        imagen_entrante = cv2.imread("LesPaul.png", 1)

cv2.destroyAllWindows()
