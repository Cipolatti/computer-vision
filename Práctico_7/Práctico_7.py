# Práctico número 7 de la materia visión por computadora
# Realiza una transformación de Similaridad

import cv2 as cv
import numpy as np
import math


def Transform_image(img, angle, x, y, scale, center=None):

    angle = np.radians(angle)

    Matriz = np.float32(
        [
            [scale * math.cos(angle), scale * math.sin(angle), x],
            [scale * (-math.sin(angle)), scale * math.cos(angle), y]
        ])

    print(Matriz)
    imagen = cv.warpAffine(img, Matriz, (1920, 1080))
    return imagen


img = cv.imread('Mate1.jpg')

while True:

    cv.imshow('Original', img)          #Shows the original image

    transf_img = Transform_image(img, 45, 500, 700, 2)
    cv.imwrite('Output.jpg', transf_img)
    cv.imshow('Transformed image', transf_img)

    key = cv.waitKey(20) & 0xFF
    if key == ord('q'):
        cv.destroyAllWindows()
        break

cv.destroyAllWindows()
