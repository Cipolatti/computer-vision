#
#

import cv2
import numpy as np
import matplotlib.pyplot as plt

MIN_MATCH_COUNT = 10

# Read two images
img_1 = cv2.imread('izq.png')
img_2 = cv2.imread('der.png')

# Transform the images to gray scale
gray_1 = cv2.cvtColor(img_1, cv2.COLOR_BGR2GRAY)
gray_2 = cv2.cvtColor(img_2, cv2.COLOR_BGR2GRAY)

# Initialize detector and descriptor
sift = cv2.xfeatures2d.SIFT_create()

# Find features and descriptors in both images
kp_1, dscr_1 = sift.detectAndCompute(gray_1, None)
kp_2, dscr_2 = sift.detectAndCompute(gray_2, None)

# Draw found points
#cv2.drawKeypoints(img_1, kp_1,img_1)
#cv2.drawKeypoints(img_2, kp_1, img_2)

matcher = cv2.BFMatcher(cv2.NORM_L2)
matches = matcher.knnMatch(dscr_1, dscr_2, k=2)

good = []
for m, n in matches:
    if m.distance < 0.7*n.distance:
        good.append(m)

if len(good) > MIN_MATCH_COUNT:
    scr_pts = np.float32([kp_1[m.queryIdx].pt for m in good]).reshape(-1, 1, 2)
    dst_pts = np.float32([kp_2[m.trainIdx].pt for m in good]).reshape(-1, 1, 2)
    H, mask = cv2.findHomography(dst_pts, scr_pts, cv2.RANSAC, 5.0)

else:
    print('Not enough matches found')
# Applies H matrix to img2
img_transformed = cv2.warpPerspective(img_2, H, (600, 800))

alpha = 0.5
blend = np.array(img_transformed * alpha + img_1 * (1 - alpha), dtype = np.uint8)
img_match = cv2.drawMatchesKnn(img_1, kp_1, img_2, kp_2, matches[:10], None,flags=0)

cv2.imshow('mezcla', blend)
cv2.imshow('transformada', img_transformed)
plt.imshow(img_match),plt.show()

'''
cv2.imshow('Original', img_1)
cv2.imshow('Movida', img_2)
'''
cv2.waitKey()

