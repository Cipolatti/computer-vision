""" Practico Nro 1 de Vision por computadora """

import random


def Adivinar(tries):

    number = random.randint(0, 100)  #Generate The random number
    print('El numero es: ', number)  #Shows the number on the screen
    print('El numero ha sido generado')
    intentos = 1
    state = True

    while state:

        guess = int(input('Ingrese el numero que piensa que se genero: '))
        if guess == number:
            print('Adivinaste en el intento numero ', intentos)
            state = False

        else:
            print('No has acertado, vuelve a intentarlo')
            if intentos >= tries:
                print('Has superado el numero de intentos')
                state = False
            else:
                intentos += 1
    else:
        print('Finish')


NumOfTries = int(input('Ingrese la cantidad de intentos: '))
Adivinar(NumOfTries)
