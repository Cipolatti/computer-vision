#Programa para probar listas 
from copy import deepcopy

list1 = [2, 2, 5, 6]
list2 = [0, 3, 7, 4]
list3 = [8, 8, 5, 2]
list4 = [1, 5, 6, 1]
FinalList = [list1, list2, list3, list4]

new_list = deepcopy(FinalList)                  #Make a copy of FinalList
print(FinalList[2])                             #Shows list2 

new_list[0][0] = 0
new_list[1][1] = 0
new_list[2][2] = 0
new_list[3][3] = 0
print(new_list)                                #Shows the FinalList with the diagonal in 0
print(FinalList)

suma = 0
for fila in FinalList:
    for num in fila:
        suma = suma + num

print(suma)                                     #Shows the result of the sum of all the nums in FinalList    


for fila in FinalList:
    for num in range(0, 4):
        if fila[num] % 2 == 1:
            fila[num] = 1
        else:
            fila[num] = 0

print(FinalList)            
