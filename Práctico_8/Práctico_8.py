#Práctico 8 de la materia visión por computadora

import cv2 as cv
import numpy as np
import math

ix, iy = -1, -1
ix_1, ix_2, iy_1, iy_2 = -1, -1, -1, -1
count = 0


def MouseEvent(event, x, y, flags, param):
    global ix, iy, ix_1, iy_1, ix_2, iy_2, count
    if event == cv.EVENT_LBUTTONDOWN:
        if count == 0:
            ix, iy = x, y
            cv.circle(img_in, (ix, iy), 4, (255, 0, 0), -1)
            count = count + 1

        elif count == 1:
            ix_1, iy_1 = x, y
            cv.circle(img_in, (ix_1, iy_1), 4, (255, 0, 0), -1)
            count = count + 1

        elif count == 2:
            ix_2, iy_2 = x, y
            cv.circle(img_in, (ix_2, iy_2), 4, (255, 0, 0), -1)
            count = 3


img_in = cv.imread('SRV.jpg')  #Imagen donde se va a incrustar otra imagen
img_in_2 = cv.imread('Mate1.jpg')  #Imagen a incrustrar
cv.namedWindow('image')
cv.setMouseCallback('image', MouseEvent)

while 1:
    cv.imshow('image', img_in)
    if count == 3:
        points = np.float32([[ix, iy], [ix_1, iy_1], [ix_2, iy_2]])
        points_1 = np.float32([[0, 0], [img_in_2.shape[1], 0],
                               [0, img_in_2.shape[0]]])
        M = cv.getAffineTransform(points_1, points)
        img_in_2_mod = cv.warpAffine(img_in_2, M,
                                     (img_in.shape[1], img_in.shape[0]))

        img2gray = cv.cvtColor(img_in_2_mod, cv.COLOR_BGR2GRAY)
        ret, mask = cv.threshold(img2gray, 3, 255, cv.THRESH_BINARY)
        mask_inv = cv.bitwise_not(mask)
        img1 = cv.bitwise_and(img_in, img_in, mask=mask_inv)
        img2 = cv.bitwise_and(img_in_2_mod, img_in_2_mod, mask=mask)
        inc = cv.add(img1, img2)

        cv.imshow('final', inc)
        count = 0
    if cv.waitKey(20) & 0xFF == 27:
        break

cv.destroyAllWindows()
