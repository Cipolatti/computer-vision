#Programa que traslada en x e y una imagen

import cv2 as cv
import numpy as np


def translate(image, x, y):
    (h, w) = (image.shape[0], image.shape[1])
    M = np.float32([[1, 0, x], [0, 1, y]])
    shifted = cv.warpAffine(image, M, (w, h))

    return shifted


img = cv.imread('Mate.jpg', 1)  #Reads the image in color
cv.imshow('BeforeTranslation', img)
print('Press "enter" for traslate the image')
cv.waitKey(0)
img_traslate = translate(img, 50, 50)
cv.imwrite('img_traslate.jpg', img_traslate)
traslated_image = cv.imread('img_traslate.jpg', 1)
cv.imshow('Traslate image', traslated_image)
cv.waitKey(0)

cv.destroyAllWindows()
