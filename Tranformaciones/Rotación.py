#Programa que lee una imagen y la rota un cierto ángulo

import cv2 as cv
import numpy as np


def rotate(image, angle, center=None, scale=1.0):
    (h, w) = image.shape[:2]

    if center is None:
        center = (w / 2, h / 2)

    M = cv.getRotationMatrix2D(center, angle, scale)
    rotated = cv.warpAffine(image, M, (w, h))

    return rotated

img = cv.imread('Mate.jpg', 1)
rotated_img = rotate(img, 180)
cv.imwrite('rotated_image.jpg', rotated_img)

