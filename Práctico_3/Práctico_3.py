#Práctico número 3 de la materia visión por computadora
#Obtención de FPS usando openCV

import sys
import cv2

if(len(sys.argv) > 1):                                  #Chequea si se pasan mas de 1 argumento
    filename = sys.argv[1]                              #El segundo argumento corresponde al nombre del archivo

else:
    print('Pass a filename as first argument')
    sys.exit(0)

cap = cv2.VideoCapture(filename)
fps = int(cap.get(5))                                   #Obtiene los fps del video capturado -- 5 = CV_CAP_PROP_FPS
print('Los fps del video son : ',fps)

while(cap.isOpened()):
    ret, frame = cap.read()
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame', gray)
    if((cv2.waitKey(fps) & 0xFF) == ord('q')):
        break

cap.release()
cv2.destroyAllWindows()

