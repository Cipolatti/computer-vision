# Práctico número 10 de Visión por Computadoras
# Detectar cuántos píxeles ocupa un objeto de medidas conocidas para determinar
# las medidas de otros objetos en la imagen

import cv2
import numpy
import math

# Returns the distance between two points
def getDistanceBetweenPoints(points_1, points_2):
    x_distance = abs(points_2[0] - points_1[0])
    y_distance = abs(points_2[1] - points_1[1])
    distance = math.sqrt((x_distance**2) + (y_distance**2))
    return distance

# Returns a list with 2 elements. Element 0 is x length of and element 1, y length
# Param: rectangle's contour
def getMiddlePointsRectangle(contourx):
    left_down_point = tuple(contourx[contourx[:, :, 0].argmin()][0])
    right_up_point = tuple(contourx[contourx[:, :, 0].argmax()][0])
    left_up_point = tuple(contourx[contourx[:, :, 1].argmin()][0])
    right_down_point = tuple(contourx[contourx[:, : , 1].argmax()][0])

    cv2.circle(final_image, left_up_point, 4, (255, 0, 0), 4)
    cv2.circle(final_image, left_down_point, 4, (255, 0, 0), 4)
    cv2.circle(final_image, right_up_point, 4, (255, 0, 0), 4)
    cv2.circle(final_image, right_down_point, 4, (255, 0, 0), 4)

    x_length = right_up_point[0] - left_down_point[0]
    y_length = right_down_point[1] - left_up_point[1]
    length_list = (x_length, y_length)
    #cv2.imshow('ventana',final_image)
    return length_list

# Returns a list with two elementes, where first one is size in X and
# second one is size in Y
def getRubberSize():
    img = image_transformed

    # Convert from RGB to HSV color space
    hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # Range for red
    lower_red = numpy.array([170, 120, 70])
    upper_red = numpy.array([190, 255, 255])
    mask1 = cv2.inRange(hsv, lower_red, upper_red)

    # Range for blue
    lower_blue = numpy.array([90, 40, 40])
    upper_blue = numpy.array([120, 255, 255])
    mask2 = cv2.inRange(hsv, lower_blue, upper_blue)

    mask = mask1 + mask2
    res1 = cv2.bitwise_and(img, img, mask=mask)
    gray = cv2.cvtColor(res1, cv2.COLOR_BGR2GRAY)
    filtered = cv2.GaussianBlur(gray, (7, 7),3)
    ret, tresh = cv2.threshold(filtered, 70, 255, cv2.THRESH_BINARY)
    edged = cv2.Canny(tresh, 70, 200)
    contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    for x, c in enumerate(contours):
        if x == 3:
            # Find max right point of the rubber
            right_point = tuple(c[c[:, : , 0].argmax()][0])
            # Find max point in Y of the rubber
            upper_point = tuple(c[c[:, : , 1].argmax()][0])
            # Find min point in Y of the rubber
            lower_point = tuple(c[c[:, : , 1].argmin()][0])
        if x == 2:
            # Find min left point of the rubber
            left_point = tuple(c[c[:, : , 0].argmin()][0])
    x_size = right_point[0] - left_point[0]
    y_size = upper_point[1] - lower_point[1]
    rubber_size = (x_size, y_size)

    # Contour 3 = Blue part of the rubber
    # Contour 2 = Red part of the rubber
    cv2.drawContours(img, contours, 2, (0, 0, 255), 2)
    cv2.circle(img, right_point, 4, (0, 0, 255), 4)
    cv2.circle(img, left_point, 4, (0, 0, 255), 4)
    cv2.circle(img, upper_point, 4, (0, 0, 255), 4)
    cv2.circle(img, lower_point, 4, (0, 0, 255), 4)

    #cv2.imshow('rubber', img)
    #cv2.waitKey(0)
    return rubber_size

# Reads the image in color
image_color = cv2.imread('imagen_práctico_10.png',1)
# Reads the image in grayscale
image = cv2.imread('imagen_práctico_10.png', 0)
# Applies a some filter
filtered_image = cv2.GaussianBlur(image, (7, 7), 3)
# Makes an umbralization of the original image
ret, tresh = cv2.threshold(filtered_image, 130, 255, cv2.THRESH_BINARY)
# Detects objects edges
tresh_edges = cv2.Canny(tresh,100,255)      #30 y 100 probar
# Invert colors (black for white and white for black). This is done cause findContours needs the image like this
invert_tresh = cv2.bitwise_not(tresh_edges)
# Find the contours of the objects in the image
contours, hierarchy = cv2.findContours(tresh_edges, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

# Calculate and print the areas of the objects. Just draw red paper contour
for x, contour in enumerate(contours):
    area = cv2.contourArea(contour)
    #print(area)
    if area > 100000 and x==53:

        # Draw the red paper contour
        #final_image = cv2.drawContours(image_color, [contour], -1, (0, 255, 0), 2)
        # Obtain the four vertices of the red paper
        left_down_point = tuple(contour[contour[:, :, 0].argmin()][0])
        right_up_point = tuple(contour[contour[:, :, 0].argmax()][0])
        left_up_point = tuple(contour[contour[:, :, 1].argmin()][0])
        right_down_point = tuple(contour[contour[:, : , 1].argmax()][0])
        # Numpy array that contains the four vertices of the red paper
        vertices_in = numpy.float32([
            [left_up_point[0], left_up_point[1]],
            [right_up_point[0],right_up_point[1]],
            [right_down_point[0], right_down_point[1]],
            [left_down_point[0], left_down_point[1]]
        ])
        # Numpy array that contains the four vertices of the red paper in the output image
        vertices_out = numpy.float32([
            [left_up_point[0], left_up_point[1]],
            [right_up_point[0], left_up_point[1]],
            [right_up_point[0], left_down_point[1]],
            [left_up_point[0], left_down_point[1]]
        ])
        # Calculate the matrix that does the transformation
        Matrix = cv2.getPerspectiveTransform(vertices_in, vertices_out)
        image_transformed = cv2.warpPerspective(image_color, Matrix, (image_color.shape[1], image_color.shape[0])) # image_transformed
        # Draw circles in the founded vertices
        cv2.circle(image_color, (left_up_point), 4, (0,0, 255), 4)
        cv2.circle(image_color, (left_down_point), 4, (0, 0, 255), 4)
        cv2.circle(image_color, (right_up_point), 4, (0, 0, 255), 4)
        cv2.circle(image_color, (right_down_point), 4, (0, 0, 255), 4)
        #cv2.imshow('testt', image_color)

#########################################################################################
# At this point i have the homography done. So i have to do all the contours again for
# the obtained homography
#####################################################################################
cv2.imshow('Homografía', image_transformed)


# Takes the homography and applied a gaussian filter to eliminate noise
filtered_image_h = cv2.GaussianBlur(image_transformed, (7, 7), 3)
# Change from color to grayscale
filtered_image_h_wb = cv2.cvtColor(filtered_image_h, cv2.COLOR_BGR2GRAY)
# Applies a binary umbralization
ret, tresh = cv2.threshold(filtered_image_h_wb, 125, 255, cv2.THRESH_BINARY)
# Canny filer to detect edges on the image
edged = cv2.Canny(tresh, 15, 180)
# Find the contours of the objects in the image
contours, hierarchy = cv2.findContours(edged, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
# Draw all the contours fonuded
final_image = cv2.drawContours(image_transformed, contours, -1, (0, 255, 0), 2)
for i, c in enumerate(contours):
    #print(cv2.contourArea(c), i)
    # i = 6, $1 coin contour
    if i == 6:
        coin_1_distance = getMiddlePointsRectangle(c)
        print('El diámetro de la moneda de 1 peso es: ','{0:.2f}'.format(coin_1_distance[0]/41.9), 'cm')
    # i = 7, $0.5 coin contour
    elif i == 7:
        coin_distance = getMiddlePointsRectangle(c)
        print('El diámetro de la moneda de 50 centavos es: ','{0:.2f}'.format(coin_distance[0]/41.9), 'cm')
    # i = 8, card contour
    elif i == 8:
        card_distance = getMiddlePointsRectangle(c)
        print('El ancho de la tarjeta es: ','{0:.2f}'.format(card_distance[0]/41.9), 'cm')
        print('El alto de la tarjeta es:  ','{0:.2f}'.format(card_distance[1]/41.9),'cm')
    # i = 9, red paper contour
    elif i == 9:
        square_distance = getMiddlePointsRectangle(c)
        print('La distancia de cada lado del papel rojo (en pixeles)  es:  ', square_distance[1])
        print(square_distance[1]/10, 'píxeles equivalen a 1 cm')
        # 1cm = 41.9 pixeles

rubber_size = getRubberSize()
print('El largo de la goma es: ', '{0:.2f}'.format(rubber_size[0]/41.9), 'cm')
print('El alto de la goma es: ' ,'{0:.2f}'.format(rubber_size[1]/41.9), 'cm')

cv2.imshow('contornos', image_transformed)
cv2.waitKey(0)

cv2.destroyAllWindows()


