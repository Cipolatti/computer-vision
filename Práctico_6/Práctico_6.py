# Práctico número 6 de la materia visión por computadora
# Realiza una transformación Euclidiana ( traslación + rotación)

import cv2 as cv
import numpy as np
import math


def Transform_image(img, angle, x, y, center=None):

    angle = np.radians(angle)

    Matriz = np.float32(
        [
            [math.cos(angle), math.sin(angle), x],
            [-math.sin(angle), math.cos(angle), y]
        ])

    print(Matriz)
    imagen = cv.warpAffine(img, Matriz, (1920, 1080))
    return imagen


img = cv.imread('Mate1.jpg')

while True:

    cv.imshow('Original', img)          #Shows the original image

    transf_img = Transform_image(img, 45, 500, 700)
    cv.imwrite('Output.jpg', transf_img)
    cv.imshow('Transformed image', transf_img)

    key = cv.waitKey(20) & 0xFF
    if key == ord('q'):
        cv.destroyAllWindows()
        break

cv.destroyAllWindows()
