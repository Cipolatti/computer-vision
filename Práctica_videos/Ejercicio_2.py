#Ejercicio práctico número 2 con videos. Como abrir un video ya creado

import sys
import cv2

if(len(sys.argv) > 1):
    filename = sys.argv[1]
else:
    print('Pass a filename as first argument')
    sys.exit(0)

cap = cv2.VideoCapture(filename)

while(cap.isOpened()):
    ret, frame = cap.read()
    
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    cv2.imshow('frame', gray)
    if((cv2.waitKey(33) & 0xFF) == ord('q')):
        break
cap.release()
cv2.destroyAllWindows()
