# Trabajo Práctico 11 de visión por computadora
# Las imágenes fueron tomadas de una simulación en Webots de una cámara montada en un brazo
# robótico. A esta cámara se le pueden agregar lentes para modificar la distorsión de 
# las imágenes. 

import cv2 
import numpy 
import glob
import matplotlib.pyplot as plt

criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 30, 0.001)
objp = numpy.zeros((4 * 3, 3), numpy.float32)
objp[:, :2] = numpy.mgrid[0:4, 0:3].T.reshape(-1,2)
# axis = numpy.float32([[3,0,0], [0,3,0], [0,0,-3]]).reshape(-1,3)

objpoints = []
imgpoints = []
# Devuelve una lista con paths de los archivos que coinciden con la búsqueda
images = glob.glob('tmp/*.png')

def draw(img, corners, imgpts):
    corner = tuple(corners[0].ravel())
    img = cv2.line(img, corner, tuple(imgpts[0].ravel()), (255,0,0), 5)
    img = cv2.line(img, corner, tuple(imgpts[1].ravel()), (0,255,0), 5)
    img = cv2.line(img, corner, tuple(imgpts[2].ravel()), (0,0,255), 5)
    return img

# Main code

for fname in images:
    img = cv2.imread(fname)
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # Busca un patrón (chessboard) con 4 x 3 esquinas internas
    ret, corners = cv2.findChessboardCorners(gray, (4, 3), None)

    if ret is True:
        
        objpoints.append(objp)
        # Realiza una búsquema mas "fina" de las esquinas
        corners2 = cv2.cornerSubPix(gray, corners, (11, 11), (-1, -1), criteria)
        imgpoints.append(corners2)
        # Dibuja las esquinas internas detectadas
        img = cv2.drawChessboardCorners(img, (4, 3), corners2, ret)
        cv2.imshow('img', img)
        cv2.waitKey(300)

# obtiene los coeficientes de distorsión de la cámara
ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
cv2.destroyAllWindows()

img = cv2.imread('test.png')
h,  w = img.shape[:2]
newcameramtx, roi=cv2.getOptimalNewCameraMatrix(mtx,dist,(w,h),1,(w,h))
# Corrige la distorsión de la cámara de la imagen que se le pasa como parámetro
dst = cv2.undistort(img, mtx, dist, None, newcameramtx)

# crop the image
x,y,w,h = roi
dst = dst[y:y+h, x:x+w]
cv2.imwrite('calibresult.png',dst)
cv2.imshow("imagen sin distorsión", dst)
cv2.imshow("imagen con distorsión", img)

cv2.waitKey()


